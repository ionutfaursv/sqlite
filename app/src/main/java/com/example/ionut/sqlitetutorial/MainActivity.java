package com.example.ionut.sqlitetutorial;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    BazaDeDate bazaDeDate;
    EditText name, text, search;
    TextView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.name);
        text = (EditText) findViewById(R.id.text);
        search = (EditText) findViewById(R.id.search);
        list = (TextView) findViewById(R.id.list);

        bazaDeDate = new BazaDeDate(this);
        refresh();
    }

    public void insertInDb(View view){
        String user =  name.getText().toString();
        String message =  text.getText().toString();

        long result = bazaDeDate.insertDB(user, message);
        if( result > 0 ){
            name.setText("");
            text.setText("");
            refresh();
            Toast.makeText(this, "Successful", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void getData(View view){
        String theName = search.getText().toString();

        String result = bazaDeDate.getTheText(theName);
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    public void refresh(){
        String theList = bazaDeDate.selectAll();
        list.setText(theList);
    }

    public void deleteUser(View view){
        int result = bazaDeDate.deleteRow();
        if( result > 0 ){
            refresh();
            Toast.makeText(this, "Deleted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Nothing to delete", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUser(View view){
        int result = bazaDeDate.updateRow();
        if( result > 0 ){
            refresh();
            Toast.makeText(this, "Updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Nothing to update", Toast.LENGTH_SHORT).show();
        }
    }
}
